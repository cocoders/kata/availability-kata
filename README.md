# Availability Kata, May 27th, 2020

## Overview

An essential constraint for the CoCoders project is to be able to match developers based on their schedules, i.e. their availability.

For two developers in the same time zone, this is pretty easy. When they cross timezones it gets harder. For this Availability Kata we will start off assuming all developers are in the same time zone.

The goal is to create a core module that can be used to match two schedules and return a number that indicates the percentage match (0 --- no match, 100 --- perfect match) for the first with respect to the second.

The Use Case is for a given developer who wants to find the best matches among a collection of potential matches.

## Setup

1) Use IntelliJ Community Edition (preferably the most recent version: 2020.1.1).

2) Clone (or fork and then clone) the repo https://gitlab.com/cocoders/kata/availability-kata (only fork if you really, really know what you're doing).

## Instructions

1) After cloning, import the top-level file `../availability-kata/build.gradle.kts` into IntelliJ (as a Project) and then set up a branch (optionally include your name or nickname for uniqueness).

2) Make sure you can execute the environment test in the file:  `../availability-kata/kata/src/commonTest/kotlin/MainTest.kt`

3) Create and run tests (red-green-refactor) to develop your solution in the file: `../availability-kata/kata/src/commonMain/kotlin/Main.kt`

A functional style is preferred over an imperative one.

Only create classes if you must. Prefer top-level functions.

Work on your solution for as long as you'd like, hours, days, weeks even. Then submit a PR for review. A totally unbiased judge will render verdict on the most readable, cleanest submission.

I will answer any and all questions to the best of my ability, via Zoom or Slack (#meetup-clean-mobile-code), chat or voice.

Picasso said: "Good artists copy, great artists steal..." For this kata, be a good or great artist and deliver a solution Uncle Bob would be proud to review. Most importantly, have fun!