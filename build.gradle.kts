plugins {
    kotlin("multiplatform") version "1.3.72"
}

group = "org.cmc.kata"
version = "1.0-SNAPSHOT"

allprojects {
    repositories {
        mavenCentral()
        jcenter()
        mavenLocal()
    }
}
